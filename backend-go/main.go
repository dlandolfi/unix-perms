package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", index)
	mux.HandleFunc("/test", test)
	fmt.Println("Listening on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", mux))

}

func index(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "this is index")
}

func test(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "this is test")
}
